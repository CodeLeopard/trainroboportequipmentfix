

local function apply(name, entity)
	if not entity.allow_robot_dispatch_in_automatic_mode then
		entity.allow_robot_dispatch_in_automatic_mode = true

		--print(name)
	end
end


for name, entity in pairs(data.raw["cargo-wagon"]) do apply(name, entity) end